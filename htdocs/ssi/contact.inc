<h3>Contact us</h3>
<p>
  We want to work with the Lepidoptera research community to build Lepbase into a 
  genuinely useful resource. If you have more data that you would like to see included or 
  want advice on how to use Lepbase in your research, please <a 
  href="mailto:contact@lepbase.org" title="contact@lepbase.org">contact us</a>.
</p>
<!--p>
  If you have found a problem with our site, please use our <a 
  href="https://bitbucket.org/lepbase/lepbase/issues">issue tracker</a> to let us know.
</p--> 