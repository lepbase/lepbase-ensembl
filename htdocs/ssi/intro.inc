<h2>the Lepidopteran genome database</h2>
  <p>The Lepidoptera comprises over 170,000 species,including major agricultural pests, 
    important plant pollinators and the first domesticated insect. The Lepidoptera have 
    played a pivotal role in the development of ecological and evolutionary biology and 
    includes ‘model’ organisms for a variety of disciplines, including conservation 
    biology, theoretical ecology, systematics, developmental biology, genetics and 
    evolutionary theory. 
  </p>
  <p>
    As research questions in the Lepidoptera are increasingly being approached using genomic
    data, Lepbase offers a platform that integrates these data, focusing on the specific 
    needs of the Lepidopteran research community to open up this diverse clade to 
    comparative analysis.
  </p>
