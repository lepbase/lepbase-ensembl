<div class="plain-box"><h2>What's new</h2>
  <div class="info-box embedded-box float-right">
    <h3>More from Lepbase...</h3>
    <p>We aim to provide a comparative genomics resource for the Lepidotera research 
      community, with BLAST and WebApollo servers in addition to this Ensembl instance.  To find out more visit 
      <a href="http://lepbase.org" title="lepbase.org">lepbase.org</a>, follow <a href="https://twitter.com/lepbase" 
    title="twitter.com/lepbase">@lepbase</a> or email us at <a href="mailto:contact@lepbase.org" 
    title="contact@lepbase.org">contact@lepbase.org</a>.
    </p>
  </div>
  <p>New species/assemblies in version 1.1:
  </p>
  <ul>
    <li>
      <em>Spodoptera frugiperda</em> v2
    </li> 
    <li>
      <em>Papilio machaon</em> papma1
    </li> 
    <li>
      <em>Papilio xuthus</em> papxu1
    </li> 
    <li>
      <em>Amyelois transitella</em> v1
    </li> 
    <li>
      <em>Operophtera brumata</em> v1 
    </li> 
    <li>
      <em>Danaus plexippus</em> v3
    </li>  
  </ul>
</div>
